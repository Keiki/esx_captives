--DecorRegister('isSurrendering', 3) -- 3 for int
DecorRegister('isCuffed', 3) -- 3 for int

ESX                           = nil
local prevFemaleVariation, prevFemaleVariation = nil, nil


Citizen.CreateThread(function()
  while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end
end)


function getAnyClosestPed()
	local pedToReturn = nil
	local player = GetPlayerPed(-1)
  local playerloc = GetEntityCoords(player, 0)
  local handle, ped = FindFirstPed()
  repeat
    success, ped = FindNextPed(handle)
    local pos = GetEntityCoords(ped)
    local distance = GetDistanceBetweenCoords(pos.x, pos.y, pos.z, playerloc['x'], playerloc['y'], playerloc['z'], true)
    if DoesEntityExist(ped) and ped ~= GetPlayerPed(-1) and distance < 2 and not IsPedInAnyVehicle(ped) and GetPedType(ped) ~= 28 and not IsPedAPlayer(ped) then
			pedToReturn = ped
    end
  until not success
  EndFindPed(handle)

	return pedToReturn
end

function ziptiePed(cuffingPed)
  local isCuffed = DecorGetInt(cuffingPed, 'isCuffed') == 1
  if not IsEntityPlayingAnim(cuffingPed, "random@arrests@busted", "idle_a", 3) then
    if isCuffed then
      ClearPedTasks(cuffingPed)
      SetEnableHandcuffs(cuffingPed, false)
      UncuffPed(cuffingPed)
      if not IsPedMale(cuffingPed) == femaleHash then -- mp female
        SetPedComponentVariation(cuffingPed, 7, prevFemaleVariation, 0, 0)
      else
        SetPedComponentVariation(cuffingPed, 7, prevMaleVariation, 0, 0)
      end
    end
    return
  end

  print('continue thread')
  if not isCuffed --[[and isSurrendering]] then
    RequestAnimDict('mp_arresting')
    while not HasAnimDictLoaded('mp_arresting') do
      Citizen.Wait(100)
    end

    ESX.ShowNotification("~g~Zip-Tieing...")
    local player = GetPlayerPed(-1) --Player
    TaskPlayAnim(player, "mp_arresting", "a_uncuff", 8.0, -8, -1, 49, 0, 0, 0, 0)
    TaskPlayAnim(cuffingPed, 'mp_arresting', 'idle', 8.0, -8, -1, 49, 0, 0, 0, 0)
    AttachEntityToEntity(cuffingPed, player, 11816, 0, 0.3, 0.0, 0.0, 0.0, 0.0, false, false, false, false, 2, true)
    Citizen.Wait (2000)
    DetachEntity(cuffingPed, true, false)
    ClearPedSecondaryTask(player)

    if not IsPedMale(cuffingPed) then -- female ped
        prevFemaleVariation = GetPedDrawableVariation(cuffingPed, 7)
        SetPedComponentVariation(cuffingPed, 7, 25, 0, 0)
    else -- male ped
        prevMaleVariation = GetPedDrawableVariation(cuffingPed, 7)
        SetPedComponentVariation(cuffingPed, 7, 41, 0, 0)
    end

    SetEnableHandcuffs(cuffingPed, true)
    SetPedCanRagdoll(cuffingPed, false) -- make him able to ragdoll
    SetBlockingOfNonTemporaryEvents(cuffingPed, true) -- makes him react to scary things (gunshots etc)
    TriggerServerEvent('pis:arr_setPedCuffed', cuffingPed, 1) -- server sync for other clients
    TriggerServerEvent('esx_captives:removeCableties')
  end

  while isCuffed do
    Citizen.Wait(0)
    if not IsEntityDead(cuffingPed) then
      TaskPlayAnim(cuffingPed, "mp_arresting", "idle", 8.0, -8, -1, 49, 0, 0, 0, 0)
      --DisablePlayerFiring(cuffingPed, true)
      TaskStandStill(cuffingPed, 1000.0)
    end
  end
end


RegisterNetEvent('esx_captives:startZiptieing')
AddEventHandler('esx_captives:startZiptieing', function()
  local player = GetPlayerPed(-1)
  local playerPos = GetEntityCoords(player)
  local cuffingPed = getAnyClosestPed()
  local closestPlayer, closestDistance = ESX.Game.GetClosestPlayer()

  if DoesEntityExist(cuffingPed) then
    ziptiePed(cuffingPed)
  elseif closestPlayer ~= -1 and closestDistance <= 3.0 then
    TriggerServerEvent('esx_policejob:handcuff', GetPlayerServerId(closestPlayer))
    TriggerServerEvent('esx_captives:removeCableties')
  end
end)
